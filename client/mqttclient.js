var host;
var port;
var clientID;
var user;
var pass;
var pubTpc;
var pubQos;
var pubRt;
var pubMsg;
var subTpc;
var subQos;
var subColor;
document.getElementsByName('pubBtn')[0].disabled = true;
document.getElementsByName('pubBtn')[0].style.backgroundColor = "#f9daac";
document.getElementsByName('subBtn')[0].disabled = true;
document.getElementsByName('subBtn')[0].style.backgroundColor = "#f9daac";
function pahoConnect()
{
var error = false;
if(document.getElementsByName('cnxHost')[0].value)
{
host = document.getElementsByName('cnxHost')[0].value;
document.getElementsByName('cnxHost')[0].style.borderColor = "initial";
}
else
{
document.getElementsByName('cnxHost')[0].style.borderColor = "red";
error = true;
}
if(parseInt(document.getElementsByName('cnxPort')[0].value))
{
port = parseInt(document.getElementsByName('cnxPort')[0].value);
document.getElementsByName('cnxPort')[0].style.borderColor = "initial";
}
else
{
document.getElementsByName('cnxPort')[0].style.borderColor = "red";
error = true;
}
if(document.getElementsByName('cnxClientId')[0].value)
{
clientID = document.getElementsByName('cnxClientId')[0].value;
document.getElementsByName('cnxClientId')[0].style.borderColor = "initial";
}
else
{
document.getElementsByName('cnxClientId')[0].style.borderColor = "red";
error = true;
}
if(error)
return;
document.getElementsByName('consoleTxt')[0].value += "Connecting...\r\n";
user = document.getElementsByName('cnxUser')[0].value;
pass = document.getElementsByName('cnxPass')[0].value;
client = new Paho.MQTT.Client(host, port, clientID);
client.onConnectionLost = onConnectionLost;
client.onMessageArrived = onMessageArrived;
if(user && pass)
{
client.connect(
   {userName: user,
    password: pass,
    onSuccess:onConnect,
    onFailure:doFail}
    );
}
else
{
client.connect({onSuccess:onConnect, onFailure:doFail});
}
}
function onConnect() {
  // Once a connection has been made, make a subscrAt w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies.iption and send a message.
  //console.log("onConnect");
document.getElementsByName('consoleTxt')[0].value += "Connection Established\r\n";
document.getElementsByName('cnxCnct')[0].style.backgroundColor = "green";
document.getElementsByName('cnxCnct')[0].innerHTML = "Connected";
document.getElementsByName('pubBtn')[0].disabled = false;
document.getElementsByName('pubBtn')[0].style.backgroundColor = "#dd9933";
document.getElementsByName('subBtn')[0].disabled = false;
document.getElementsByName('subBtn')[0].style.backgroundColor = "#dd9933";
}
// called when the client loses its connection
function onConnectionLost(responseObject) {
  if (responseObject.errorCode !== 0) {
    document.getElementsByName('consoleTxt')[0].value += "Connection Lost: " + responseObject.errorMessage + "\r\n";
    document.getElementsByName('cnxCnct')[0].style.backgroundColor = "red";
    document.getElementsByName('cnxCnct')[0].innerHTML = "ReConnect";
    document.getElementsByName('pubBtn')[0].disabled = true;
    document.getElementsByName('pubBtn')[0].style.backgroundColor = "#f9daac";
    document.getElementsByName('subBtn')[0].disabled = true;
    document.getElementsByName('subBtn')[0].style.backgroundColor = "#f9daac";
  }
}
// called when a message arrives
function onMessageArrived(message) {
  document.getElementsByName('consoleTxt')[0].value += "RX: "+ message.payloadString + "\r\n";
}
// called when fail
function doFail(responseObject) {
  document.getElementsByName('consoleTxt')[0].value += "Connection Failed: "+ responseObject.errorMessage + "\r\n";
  document.getElementsByName('pubBtn')[0].disabled = true;
  document.getElementsByName('pubBtn')[0].style.backgroundColor = "#f9daac";
  document.getElementsByName('subBtn')[0].disabled = true;
  document.getElementsByName('subBtn')[0].style.backgroundColor = "#f9daac";
}
function pahoPublish() {
if(document.getElementsByName('pubTopic')[0].value)
  pubTpc = document.getElementsByName('pubTopic')[0].value;
pubQos = document.getElementsByName('pubQos')[0].value;
if(document.getElementsByName('pubRetain')[0].value)
  pubRt = document.getElementsByName('pubRetain')[0].checked;
if(document.getElementsByName('pubMessage')[0].value)
  pubMsg = document.getElementsByName('pubMessage')[0].value;
  document.getElementsByName('consoleTxt')[0].value += "Publishing..."+ "Topic: " + pubTpc + " | Msg: " + pubMsg + " | QOS: " + pubQos + " | Rt: " + pubRt + "\r\n";
  message = new Paho.MQTT.Message(pubMsg);
  message.destinationName = pubTpc;
  message.qos = parseInt(pubQos);
  message.retained = pubRt;
  client.send(message);
}
function pahoSubscribe() {
if(document.getElementsByName('subTopic')[0].value)
  subTpc = document.getElementsByName('subTopic')[0].value;

document.getElementsByName('subTxt')[0].value += subTpc + "\r\n";

}
function pahoLazy() {
host = "mqtt.lazyengineers.com";
port = 10452;
clientID = "lazy_"+ parseInt(Math.random() * 100000, 10);
user = "lazyengineers";
pass = "lazyengineers";
pubTpc = "lazy/engineers";
pubQos = 0;
pubRt = 0;
pubMsg = "Lazy Engineers Are Awesome";
subTpc = "lazy/engineers";
subQos = 0;
subColor = "black";
document.getElementsByName('cnxHost')[0].value=host;
document.getElementsByName('cnxPort')[0].value=port;
document.getElementsByName('cnxUser')[0].value=user;
document.getElementsByName('cnxPass')[0].value=pass;
document.getElementsByName('cnxClientId')[0].value=clientID;
document.getElementsByName('pubTopic')[0].value=pubTpc;
document.getElementsByName('pubMessage')[0].value=pubMsg;
document.getElementsByName('subTopic')[0].value=subTpc;
}
   function addItem()
   {
     if(textInp.value)
     {
       var arr = textInp.value.split(",");
       textInp.value = "";
       subQos = document.getElementsByName('subQos')[0].value;
       for(var i=0; i<arr.length; i++)
       {
	 if(getList().indexOf(arr[i].trim()) > -1)
	 {	
	   document.getElementsByName('consoleTxt')[0].value += "Already Subscribed...\r\n"
           return;
	 }
         divHTML.firstChild.lastChild.innerHTML = arr[i].trim();
         var html = divHTML.innerHTML;
         divList.appendChild(divHTML.firstChild);
         divHTML.innerHTML = html;
	 subTpc = arr[i].trim();
	 document.getElementsByName('consoleTxt')[0].value += 
                                    "Subscribing..."+ "Topic: " + subTpc + "\r\n";
         client.subscribe(subTpc, {qos: parseInt(subQos)});
       }
     }
     else
       alert("Please enter a text");
   }
   function getList()
   {
     var arr = [];
     for(var i=0; i<divList.childNodes.length; i++) arr.push(divList.childNodes[i].lastChild.innerHTML);
     return arr;
   }
   function rmItem(params)
   {	
	client.unsubscribe(params.val);
	console.log("removing " + params.val);
   }
